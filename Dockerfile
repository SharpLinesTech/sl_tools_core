FROM alpine:3.9

RUN apk add --no-cache g++ make cmake git

RUN mkdir /usr/sl_src && \
    mkdir /usr/sl_bld

#Gather deps is a separate copy so that it gets cached
COPY ./gather_deps.sh /usr/sl_src/gather_deps.sh
RUN /usr/sl_src/gather_deps.sh /usr/local

COPY . /usr/sl_src

WORKDIR /usr/sl_bld
RUN mkdir sl_tools && cd sl_tools && \
    cmake -DCMAKE_BUILD_TYPE=RELEASE -DCMAKE_INSTALL_PREFIX=/usr/local /usr/sl_src && \
    make -j $(nproc) && make install

WORKDIR /home

RUN rm -rf /usr/sl_src && \
    rm -rf /usr/sl_bld