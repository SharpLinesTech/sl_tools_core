#include "slt/core.h"
#include "slt/settings.h"

#include "slt/concur/work_pool.h"
#include "slt/file/packed_file.h"
#include "slt/file/read.h"
#include "slt/file/write.h"
#include "slt/flat_hash_table.h"
#include "slt/mem/read_buf.h"

#include "slt/mem/alignment.h"

slt::Setting<std::string> dir("dir", "",
                              "A file containing the list of files to pack");
slt::Setting<std::string> dst("dst", "", "Where to store the result");

struct file_info {
  slt::Data_block data;
  std::size_t offset;
};

int main(int argc, const char* argv[]) {
  slt::Core core(argc, argv);

  try {
    slt::concur::Work_pool work(1, 1);
    work.make_current();

    std::mutex files_mutex;
    std::unordered_map<std::uint64_t, file_info> files;

    auto dir_file = slt::file::async_read(dir.get());
    dir_file.then(
      [&](slt::Data_block d) -> void{
        slt::Read_buf reader((char*)d.data(), d.size());
        std::istream stream(&reader);

        std::string name;
        std::string local_file;

        while (stream >> name >> local_file) {
          auto file_contents = slt::file::async_read(local_file);
          file_contents.then([name, &files, &files_mutex](slt::Data_block data) -> void {
            auto l = std::lock_guard(files_mutex);

            if (name == "__import__") {
              slt::file::In_memory_packed_file file(data);  
              for (auto f : file) {
                if (files.find(f.first) != files.end()) {
                  throw std::runtime_error("hash collision!");
                }
                files[f.first].data =
                    slt::Data_block(f.second);
              }
            } else {
              auto hash_val = std::hash<std::string>{}(name);
              if (files.find(hash_val) != files.end()) {
                throw std::runtime_error("hash collision!");
              }
              files[hash_val].data =
                  std::move(data);
            }
          });
        }
      });

    work.wait_idle();

    std::vector<std::pair<std::uint64_t, slt::file::packed_file_info>> pack_info;

    std::size_t total_data_size = 0;
    for (auto& f : files) {
      total_data_size = slt::mem::align_size(total_data_size, 8);

      auto file_size = f.second.data.size();
      pack_info.emplace_back(
          f.first, slt::file::packed_file_info{uint32_t(total_data_size),
                                               uint32_t(file_size)});
      total_data_size += file_size;
    }

    auto header = slt::bake_flat_hash_table(pack_info);
    slt::Flat_hash_table<slt::file::packed_file_info> header_map(header.data(),
                                                                 header.size());

    auto header_size = header.size();
    header_size = slt::mem::align_size(header_size, 8);

    // Start building the final data buffer.

    // The header data will be updated with the correct offsets as we build the
    // final buffer, so it's memcopy'd last.
    slt::Data_block final_data(header_size + total_data_size);

    std::size_t write_offset = header_size;

    for (auto& f : files) {
      std::uint32_t size = std::uint32_t(f.second.data.size());
      std::memcpy(final_data.data() + write_offset, f.second.data.data(), size);

      header_map.at_key(f.first).offset = std::uint32_t(write_offset);
      write_offset += size;
      write_offset = slt::mem::align_size(write_offset, 8);
    }

    std::memcpy(final_data.data(), header.data(), header.size());
    slt::file::write(final_data, dst.get());
  } catch (std::exception& e) {
    std::cerr << e.what() << "\n";
    return 1;
  }
  return 0;
}