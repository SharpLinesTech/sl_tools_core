#include "slt/log.h"

#include "slt/settings.h"

#include "spdlog/sinks/stdout_sinks.h"

namespace {
std::shared_ptr<spdlog::logger> createLogger(std::string name) {
  return spdlog::stderr_logger_mt(name);
}
}  // namespace

namespace slt {
std::shared_ptr<spdlog::logger> log;

namespace logging {
std::shared_ptr<spdlog::logger> init_log;

void preInit() { init_log = createLogger("SLT_init"); }

void init() {
  init_log = nullptr;
  spdlog::drop("SLT_init");

  log = createLogger("SLT");
  log->info("Logging initiated");
}

void shutdown() {
  spdlog::drop("SLT");
  log->info("Logging terminated");
  log->flush();

  log = nullptr;
}
}  // namespace logging

namespace settings {
Setting<bool> log_async(
    "log_async", false,
    "Enable async logging. Less impact on performance, but less consistent");

Setting<int32_t> async_log_queue(
    "async_log_queue", 4096,
    "If async logging is enabled, sets the length of the async buffer.",
    [](int32_t const& v) { return (v & (v - 1)) == 0; });
}  // namespace settings
}  // namespace slt
