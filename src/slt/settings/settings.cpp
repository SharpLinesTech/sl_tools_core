#include "slt/settings.h"
#include "slt/debug/assert.h"
#include "slt/file/read.h"
#include "slt/json.h"
#include "slt/log.h"
#include "slt/log/log_internal.h"
#include "slt/settings/settings_internal.h"

#include <iomanip>
#include <iostream>
#include <map>
#include <string>

namespace {
using settings_store_t = std::unordered_map<std::string, ::slt::Setting_base*>;
settings_store_t& settingsStore() {
  static settings_store_t instance;
  return instance;
}
}  // namespace

namespace slt {

namespace settings {

void init(int argc, const char* argv[]) {
  logging::init_log->info("Parsing options:");
  auto& store = settingsStore();

  for (int i = 1; i < argc; ++i) {
    std::string_view arg = argv[i];
    if (arg.size() > 2 && arg[0] == '-' && arg[1] == '-') {
      arg = arg.substr(2);
      std::string_view name = arg;
      std::string_view val;

      auto eq_pos = arg.find('=');
      if (eq_pos != std::string_view::npos) {
        name = arg.substr(0, eq_pos);
        val = arg.substr(eq_pos + 1);
      } else {
        if (name.find("no_") == 0) {
          name = name.substr(3);
          val = "false";
        } else {
          val = "true";
        }
      }
      logging::init_log->info("setting found: {}", name);
      auto setting = store.find(std::string(name));
      if (setting == store.end()) {
        throw std::runtime_error("not a setting");
      }

      setting->second->set_from_string(val);
    }
  }

  logging::init_log->info("Done parsing options");
}

void resetAll() {
  for (auto& setting : settingsStore()) {
    setting.second->reset();
  }
}

void load_setting_file(const std::string& filename) {
  logging::init_log->info("loading config file: {}", filename);

  auto data = file::read(filename);
  auto j = json::parse(data.begin(), data.end());
  auto& store = settingsStore();

  for (auto const& [key, val] : j.items()) {
    logging::init_log->info("setting found: {}", key);
    auto setting = store.find(key);
    if (setting == store.end()) {
      throw std::runtime_error("not a setting");
    }

    setting->second->set_from_string(val.dump());
  }
}

void display_help() {
  std::cout << "Settings:\n";

  std::vector<std::pair<std::string, Setting_base*>> sorted_settings;
  for (const auto& s : settingsStore()) {
    sorted_settings.emplace_back(s.first, s.second);
  }
  std::sort(sorted_settings.begin(), sorted_settings.end(),
            [](auto const& l, auto const& r) { return l.first < r.first; });

  for (const auto& s : sorted_settings) {
    std::cout << "  --" << s.second->help_string() + "\n";
  }
}
}  // namespace settings

void Setting_base::registerSetting(Setting_base* setting) {
  auto& store = settingsStore();

  if (store.find(setting->name()) != store.end()) {
    throw Settings_error(fmt::format(
        "trying to register the same setting twice: {}", setting->name()));
  }

  store[setting->name()] = setting;
}

}  // namespace slt