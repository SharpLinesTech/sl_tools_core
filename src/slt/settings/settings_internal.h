#ifndef SLT_SETTINGS_INTERNAL_H
#define SLT_SETTINGS_INTERNAL_H

#include <string_view>
#include <vector>

namespace slt {
namespace settings {
void init(int argc, const char* argv[]);

void display_help();
void load_setting_file(const std::string& filename);
// reset all registered settings to their default values.
void resetAll();
}  // namespace settings
}  // namespace slt

#endif