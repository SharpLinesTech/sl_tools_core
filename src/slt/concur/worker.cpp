#include "slt/concur/worker.h"
#include "slt/log.h"

namespace slt {

namespace concur {

Worker::Worker(Job_queue* src) : src_(src), thread_(&Worker::main, this) {
  slt::log->info("worker starting");
}

Worker::~Worker() {
  term_ = true;
  src_->push(nullptr);
  thread_.join();
  slt::log->info("worker finished");
}

void Worker::main() {
  while (1) {
    auto op = src_->pop();
    if (!op) {
      // This is a little messier than strictly required, but this
      // allows us to add/remove workers on the fly from a given pool.
      if (term_) {
        break;
      } else {
        src_->push(nullptr);
        std::this_thread::yield();
        continue;
      }
    }
    op();
  }
}
}  // namespace concur
}  // namespace slt