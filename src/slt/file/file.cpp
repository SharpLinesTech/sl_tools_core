#include <fstream>

#include "slt/concur/worker.h"
#include "slt/core.h"
#include "slt/file/file_internal.h"
#include "slt/file/read.h"
#include "slt/file/write.h"
#include "slt/log.h"
#include "slt/log/log_internal.h"

namespace {
  std::unique_ptr<slt::concur::Work_pool> fs_pool;
}  // namespace

namespace slt {
namespace file {

Read_error::Read_error(std::string reason) : std::runtime_error(reason) {}
Write_error::Write_error(std::string reason) : std::runtime_error(reason) {}

void startFilesystemThread() {
  slt::log->info("starting file system worker");
  fs_pool = std::make_unique<slt::concur::Work_pool>(1, 1);
}

void stopFilesystemThread() {
  slt::log->info("killing file system worker");
  fs_pool.reset();
}

Data_block read(std::string const &file) {
  std::ifstream in_f(file.c_str(), std::ios::binary);
  if (!in_f.good()) {
    log->error("File load failure");
    throw Read_error(fmt::format("Failed to load file: {}", file));
  }

  in_f.seekg(0, in_f.end);
  auto length = in_f.tellg();

  if (log) {
    log->info("Read {} bytes from file: {}", length, file);
  } else if (logging::init_log) {
    logging::init_log->info("Read {} bytes from file: {}", length, file);
  }

  in_f.seekg(0, in_f.beg);
  Data_block file_contents(length);
  in_f.read(reinterpret_cast<char *>(file_contents.data()), length);
  return file_contents;
}

slt::Future<Data_block> async_read(std::string file) {
  slt::Promise<Data_block> p;
  
  auto result = p.get_future();
  
  fs_pool->push_job([file{move(file)}, p{move(p)}]() mutable {
    Data_block data;
    try {
      p.set_value(slt::file::read(file));
    } catch (...) {
      p.set_exception(std::current_exception());
    }
  });

  return result;
}

void write(Const_data_view data, std::string_view file) {
  slt::log->info("Writing to file: {}", file);

  std::ofstream out_file(std::string(file), std::ios::binary);

  if (!out_file.good()) {
    throw Write_error(fmt::format("Failed to write file: {}", file));
  }

  slt::log->info("Wrote {} bytes", data.size);

  out_file.write(data.data, data.size);
}

void throw_read_error(slt::file::Read_error e) { throw e; }

}  // namespace file
}  // namespace slt