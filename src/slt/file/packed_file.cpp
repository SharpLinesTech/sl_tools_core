#include "slt/file/packed_file.h"

namespace slt {
namespace file {

In_memory_packed_file::In_memory_packed_file(Data_view data)
    : data_(data), contents_(data_.data, data.size) {
  /*
      for(auto const& d : contents_) {
        if(d.second.offset + d.second.length > data_.size()) {
          throw std::runtime_error("invalid packed file");
        }
      }
      */
}

Data_view In_memory_packed_file::get(std::string_view name) const {
  auto info = contents_.at(name);
  return Data_view(data_.data + info.offset, info.length);
}

}  // namespace file
}  // namespace slt
