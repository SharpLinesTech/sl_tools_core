#include "slt/core.h"

#include "slt/debug/assert.h"
#include "slt/file/file_internal.h"
#include "slt/log/log_internal.h"
#include "slt/settings/settings_internal.h"

namespace {
std::vector<std::string_view> argvToArgs(int argc, const char* argv[]) {
  std::vector<std::string_view> args(argc - 1);
  for (int i = 1; i < argc; ++i) {
    args[i - 1] = argv[i];
  }
  return args;
}
}  // namespace

namespace slt {
namespace settings {
Setting<std::string> config_file("config", "", "config file to load");
Setting<bool> request_help("help", false, "Shows this help");

}  // namespace settings

Core* Core::instance = nullptr;

Core::Core() : Core(0, nullptr) {}

Core::Core(int argc, const char* argv[]) {
  // There should only ever be a single core instance in existence.
  SLT_ASSERT(instance == nullptr);
  instance = this;
  logging::preInit();

  if (argc > 0) {
    settings::init(argc, argv);

    if (settings::config_file.get() != "") {
      settings::load_setting_file(settings::config_file.get());
    }

    if (settings::request_help.get()) {
      settings::display_help();
      std::exit(EXIT_SUCCESS);
    }
  }

  logging::init();

  file::startFilesystemThread();
}

Core::~Core() {
  SLT_ASSERT(instance == this);

  file::stopFilesystemThread();

  settings::resetAll();
  logging::shutdown();

  instance = nullptr;
}

}  // namespace slt
