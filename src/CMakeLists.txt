include_directories(.)

add_library(slt_core
  slt/concur/work_pool.cpp
  slt/concur/worker.cpp
  slt/debug/assert.cpp
  slt/file/file.cpp
  slt/file/packed_file.cpp
  slt/log/log.cpp
  slt/settings/settings.cpp

  slt/core.cpp
)

target_link_libraries(slt_core Threads::Threads spdlog::spdlog fmt::fmt-header-only)

target_include_directories(slt_core PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/../include>
  $<INSTALL_INTERFACE:$<INSTALL_PREFIX>/include>)

target_compile_features(slt_core PUBLIC cxx_std_17)

if(WIN32)
  target_compile_definitions(slt_core 
  PUBLIC
    _WIN32_WINNT=0x0501
    _SILENCE_CXX17_ALLOCATOR_VOID_DEPRECATION_WARNING
    _ITERATOR_DEBUG_LEVEL=0
  )
endif()


install(TARGETS slt_core
  EXPORT   sl_tools_coreTargets
  ARCHIVE  DESTINATION lib
  LIBRARY  DESTINATION lib
  RUNTIME  DESTINATION bin
)