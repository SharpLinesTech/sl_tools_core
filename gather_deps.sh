#!/bin/sh

REF_PATH=$(pwd)
DST_PATH=$(pwd)/$1
BUILD_TYPE=RELEASE

git clone --depth=1 --single-branch --branch 5.3.0 https://github.com/fmtlib/fmt.git
mkdir fmt_bld
cd fmt_bld
cmake -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DFMT_DOC=OFF -DFMT_TEST=OFF -DCMAKE_INSTALL_PREFIX=$DST_PATH ../fmt
make -j $(nproc) && make install
cd $REF_PATH
rm -rf fmt
rm -rf fmt_bld

git clone --depth=1 --single-branch --branch v1.3.1 https://github.com/gabime/spdlog.git
mkdir spdlog_bld
cd spdlog_bld
cmake -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_PREFIX_PATH=$DST_PATH -DSPDLOG_BUILD_EXAMPLES=OFF -DSPDLOG_BUILD_BENCH=OFF -DSPDLOG_BUILD_TESTS=OFF -DSPDLOG_FMT_EXTERNAL=ON -DCMAKE_INSTALL_PREFIX=$DST_PATH ../spdlog
make -j $(nproc) && make install
cd $REF_PATH
rm -rf spdlog
rm -rf spdlog_bld

git clone --depth=1 --single-branch --branch v3.5.0 https://github.com/nlohmann/json.git
mkdir json_bld
cd json_bld
cmake -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DJSON_BuildTests=OFF -DCMAKE_INSTALL_PREFIX=$DST_PATH ../json
make -j $(nproc) && make install
cd $REF_PATH
rm -rf json
rm -rf json_bld



