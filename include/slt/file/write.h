#ifndef SLT_FILE_WRITE_H
#define SLT_FILE_WRITE_H

#include <string_view>
#include "slt/mem/data_view.h"

namespace slt {
namespace file {
struct Write_error : public std::runtime_error {
  Write_error(std::string);
};

// Synchronously writes a chunk of data to a file.
void write(Const_data_view data, std::string_view file);

}  // namespace file
}  // namespace slt

#endif
