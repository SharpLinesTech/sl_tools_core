#ifndef SLT_PACKED_FILE_H
#define SLT_PACKED_FILE_H

#include "slt/flat_hash_table.h"
#include "slt/mem/data_block.h"
#include "slt/mem/data_view.h"

#include <cstdint>
#include <fstream>
#include <unordered_map>

namespace slt {
namespace file {

struct packed_file_info {
  std::uint32_t offset;
  std::uint32_t length;
};

class In_memory_packed_file {
 public:
  In_memory_packed_file(Data_view data);
  Data_view get(std::string_view name) const;

  struct iterator {
    Flat_hash_table<packed_file_info>::iterator ite;
    Data_view data_;

    bool operator==(const iterator& rhs) { return ite == rhs.ite; }

    bool operator!=(const iterator& rhs) { return ite != rhs.ite; }

    iterator& operator++() {
      ++ite;
      return *this;
    }

    std::pair<std::uint64_t, Data_view> operator*() const {
      auto info = ite->second;
      return std::make_pair(ite->first,
                            Data_view(data_.data + info.offset, info.length));
    }
  };

  iterator begin() { return iterator{contents_.begin(), data_}; }

  iterator end() { return iterator{contents_.end(), data_}; }

 private:
  Data_view data_;
  Flat_hash_table<packed_file_info> contents_;
};

}  // namespace file
}  // namespace slt

#endif
