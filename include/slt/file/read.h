#ifndef SLT_FILE_READ_H
#define SLT_FILE_READ_H

#include <functional>
#include <stdexcept>
#include "slt/concur/blocking_queue.h"
#include "slt/concur/event_queue.h"
#include "slt/concur/future.h"
#include "slt/concur/work_pool.h"
#include "slt/mem/data_block.h"

namespace slt {

namespace file {

// Thrown/returned when something goes wrong when attempting to read a file.
struct Read_error : public std::runtime_error {
  Read_error(std::string);
};

//
using Read_callback = std::function<void(Data_block)>;
using Stream_read_finished_callback = std::function<void(void)>;
using Read_failed_callback = std::function<void(Read_error)>;

// Asynchronously loads file from the native filesystem.
slt::Future<Data_block> async_read(std::string file);

// Asynchronously loads file from the native filesystem in packets.
// This can be usefull when loading very large files to avoid having
// to make very large allocations.
void asyncStreamRead(std::string file, concur::Event_queue &queue,
                     std::size_t packet_size, Read_callback cb,
                     Stream_read_finished_callback fin_cb,
                     Read_failed_callback err_cb = nullptr);

// A convenience Read_failed_callback that simply throws the error.
void throw_read_error(slt::file::Read_error);

// You shouldn't need this. It's only required for super-early initialization.
Data_block read(std::string const &file);
}  // namespace file
}  // namespace slt

#endif
