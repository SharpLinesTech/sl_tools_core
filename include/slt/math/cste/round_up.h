#ifndef SLT_CSTE_MATH_ROUND_UP_H_INCLUDED
#define SLT_CSTE_MATH_ROUND_UP_H_INCLUDED

#include "slt/math/cste/inf_nan.h"
#include "slt/math/cste/power.h"

#include <cmath>
#include <limits>

namespace slt {
namespace cste {

template <typename T>
constexpr T round_up(const T& v) {
  constexpr T range_max = power(T(2), (std::numeric_limits<T>::digits - 1));
  constexpr T range_min = -range_max;

  if (v >= range_max || v <= range_min || is_nan(v)) {
    return v;
  }

  long long int x = static_cast<long long int>(v);
  if (v == T(x) || v < T(0)) {
    return T(x);
  }
  return T(x + 1);
}
}  // namespace cste
}  // namespace slt

#endif
