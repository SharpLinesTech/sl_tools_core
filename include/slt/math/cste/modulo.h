#ifndef SLT_CSTE_MATH_MODULO_H_INCLUDED
#define SLT_CSTE_MATH_MODULO_H_INCLUDED

#include "slt/math/cste/round_down.h"

#include <type_traits>

namespace slt {
namespace cste {
template <typename T>
constexpr T modulo(const T& val, const T& div) {
  if constexpr (std::is_integral_v<T>) {
    return val % div;
  } else {
    return val - round_down(val / div) * div;
  }
}
}  // namespace cste
}  // namespace slt

#endif