#ifndef SLT_CSTE_MATH_ROUNDING_ABSOLUTE_H_INCLUDED
#define SLT_CSTE_MATH_ROUNDING_ABSOLUTE_H_INCLUDED

namespace slt {
namespace cste {
template <typename T>
constexpr T absolute(const T& v) {
  return v < 0 ? -v : v;
}
}  // namespace cste
}  // namespace slt

#endif
