#ifndef SLT_CSTE_MATH_TRUNCATE_H_INCLUDED
#define SLT_CSTE_MATH_TRUNCATE_H_INCLUDED

#include "slt/cste_math/calc/power.h"
#include "slt/cste_math/misc/inf_nan.h"

#include <cmath>
#include <limits>

namespace slt {
namespace cste {

template <typename T>
constexpr T truncate(const T& v) {
  long long int x = static_cast<long long int>(v);
  return T(x);
}
}  // namespace cste
}  // namespace slt

#endif