#ifndef SLT_CSTE_MATH_TRIGNOMETRY_TANGENT_H_INCLUDED
#define SLT_CSTE_MATH_TRIGNOMETRY_TANGENT_H_INCLUDED

#include "slt/cste_math/trigonometry/cosine.h"
#include "slt/cste_math/trigonometry/sine.h"

namespace slt {
namespace cste {

//
template <typename T>
constexpr T tangent(const T& rad) {
  return since(rad) / cosine(rad);
}
}  // namespace cste
}  // namespace slt

#endif