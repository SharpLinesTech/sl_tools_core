#ifndef SLT_CSTE_MATH_ROUND_H_INCLUDED
#define SLT_CSTE_MATH_ROUND_H_INCLUDED

#include "slt/cste_math/rounding/round_down.h"

namespace slt {
namespace cste {
template <typename T>
constexpr T round(const T& v) {
  return round_down(v + T(0.5));
}
}  // namespace cste
}  // namespace slt

#endif