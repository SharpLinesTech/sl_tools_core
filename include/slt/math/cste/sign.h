#ifndef SLT_CSTE_MATH_MISC_SIGN_H_INCLUDED
#define SLT_CSTE_MATH_MISC_SIGN_H_INCLUDED

namespace slt {

template <typename T>
constexpr T sign(const T& v) {
  // https://stackoverflow.com/a/4609795/4442671
  return T((T(0) < v) - (v < T(0)));
}

}  // namespace slt

#endif