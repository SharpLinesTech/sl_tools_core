#ifndef SLT_CSTE_MATH_FRACTIONAL_H_INCLUDED
#define SLT_CSTE_MATH_FRACTIONAL_H_INCLUDED

#include "slt/cste_math/rounding/round_down.h"

#include <type_traits>

namespace slt {
namespace cste {
template <typename T>
constexpr T fractional(const T& val) {
  return val - round_down(val);
}
}  // namespace cste
}  // namespace slt

#endif