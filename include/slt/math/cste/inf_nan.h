#ifndef SLT_CSTE_MATH_MISC_NAN_H_INCLUDED
#define SLT_CSTE_MATH_MISC_NAN_H_INCLUDED

namespace slt {
namespace cste {
template <typename T>
constexpr bool is_nan(const T& v) {
  return !(v == v);
}

template <typename T>
constexpr bool is_inf(const T& v) {
  constexpr T inf = std::numeric_limits<T>::infinity();
  return v == inf || v == -inf;
}
}  // namespace cste
}  // namespace slt

#endif