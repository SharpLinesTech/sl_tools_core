//  Copyright 2018 Francois Chabot
//  (francois.chabot.dev@gmail.com)
//
//  Distributed under the Boost Software License, Version 1.0.
//  (See accompanying file LICENSE or copy at
//  http://www.boost.org/LICENSE_1_0.txt)

#ifndef VECPP_ANGLE_OPERATIONS_H_INCLUDED
#define VECPP_ANGLE_OPERATIONS_H_INCLUDED

#include "slt/math/angle/angle.h"

#include <cmath>

namespace slt {

// Trigonometry

// TODO: These placeholder taylor series expansion implementation
// are temporary, and need to be replaced with something better!
template <typename T, typename Traits>
constexpr T sin(const Angle<T, Traits>& a) {
  return std::sin(a.as_rad());
}

template <typename T, typename Traits>
constexpr T cos(const Angle<T, Traits>& a) {
  return std::cos(a.as_rad());
}

template <typename T, typename Traits>
constexpr T tan(const Angle<T, Traits>& a) {
  return std::tan(a.as_rad());
}

}  // namespace slt

#endif
