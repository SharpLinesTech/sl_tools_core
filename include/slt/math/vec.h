#ifndef SLT_MATH_VEC_H
#define SLT_MATH_VEC_H

#include <iostream>

#include "slt/math/angle/angle.h"
#include "slt/math/mat/mat.h"
#include "slt/math/quat/quat.h"
#include "slt/math/vec/vec.h"

namespace slt {

// While we are using glm for most math operations, everything goes through this
// file so that the library can be swapped out.

template <typename T>
using Vec2 = Vec<T, 2>;

template <typename T>
using Vec3 = Vec<T, 3>;

template <typename T>
using Vec4 = Vec<T, 4>;

template <typename T>
using Mat4 = Mat<T, 4, 4>;

using iVec2 = Vec2<int>;
using iVec3 = Vec3<int>;
using iVec4 = Vec4<int>;

using fVec2 = Vec2<float>;
using fVec3 = Vec3<float>;
using fVec4 = Vec4<float>;

using fMat4 = Mat4<float>;
using fQuat = Quat<float>;

using fAngle = Angle<float>;
// using Mat2 = glm::mat2;
// using Mat3 = glm::mat3;
// using Mat4 = glm::mat4;

}  // namespace slt
#endif