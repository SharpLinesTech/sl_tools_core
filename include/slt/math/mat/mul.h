//  Copyright 2018 Francois Chabot
//  (francois.chabot.dev@gmail.com)
//
//  Distributed under the Boost Software License, Version 1.0.
//  (See accompanying file LICENSE or copy at
//  http://www.boost.org/LICENSE_1_0.txt)

#ifndef VECPP_MAT_MUL_H_INCLUDED
#define VECPP_MAT_MUL_H_INCLUDED

#include "slt/math/mat/mat.h"

namespace slt {

template <typename T, std::size_t N, std::size_t M, std::size_t P,
          typename traits>
constexpr Mat<T, P, N, traits> operator*(const Mat<T, M, N, traits>& lhs,
                                         const Mat<T, P, M, traits>& rhs) {
  Mat<T, P, N, traits> result = {};

  for (std::size_t i = 0; i < P; ++i) {
    for (std::size_t j = 0; j < N; ++j) {
      T v = T(0);

      for (std::size_t k = 0; k < M; ++k) {
        v += lhs(k, j) * rhs(i, k);
      }

      result(i, j) = v;
    }
  }

  return result;
}

}  // namespace slt

#endif