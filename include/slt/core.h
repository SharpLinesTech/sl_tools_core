#ifndef SLT_CORE_CORE_H
#define SLT_CORE_CORE_H

#include <future>
#include <string_view>
#include <vector>

#include "slt/concur/event_queue.h"
#include "slt/concur/worker.h"
#include "slt/settings.h"

namespace slt {

// There can never be more that one slt core active at any point in time,
// accessible through Core::instance. Furthermore, many things assume the
// core instance to be set, so setting up a core is generally the first thing
// done at the top of main().

// Core provides 3 functionalities:
// 1. Initialize settings.
//     - Settings are reset to default values on destruction, to facilitate
//       unit testing.
// 2. Initializes logging.
// 3. Initializes asynchronous filesystem

namespace settings {
extern Setting<int> workers_count;
}

enum class Task_handler {
  main_queue,
  worker,
};

struct [[nodiscard]] Core {
  Core();
  Core(int, const char* []);

  ~Core();

  static Core* instance;

  concur::Event_queue& main_queue() { return main_queue_; }

 private:
  concur::Event_queue main_queue_;
};

}  // namespace slt

#endif
