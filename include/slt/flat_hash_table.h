#ifndef SLT_FLAT_HASH_TABLE_H
#define SLT_FLAT_HASH_TABLE_H

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <cstring>
#include <functional>
#include <type_traits>
#include <unordered_set>
#include <utility>
#include <vector>

namespace slt {
// The flat hash table is meant to be used when a hash table is baked once,
// typically during a build process, and then used repeatadly. It can be
// initialized by simply pointing it at a memory location containing the
// raw data.
// Obviously, it's an inherently read-only structure.
template <typename ValT, typename HashT = std::uint64_t>
class Flat_hash_table {
  static_assert(std::is_trivial_v<ValT>);
  static_assert(std::is_trivial_v<HashT>);

 public:
  // This cannot be a std::pair<> because the default constructor is not trivial
  struct elem_t {
    HashT first;
    ValT second;
  };

  using iterator = const elem_t*;

  Flat_hash_table(char* mem_loc, std::size_t mem_len) : mem_loc_(mem_loc) {
    if (mem_len < sizeof(bucket_count_)) {
      throw std::invalid_argument("invalid flat hash data");
    }

    char* read_ptr = mem_loc;
    std::memcpy(&bucket_count_, read_ptr, sizeof(bucket_count_));
    read_ptr += sizeof(bucket_count_);

    if (mem_len < sizeof(bucket_count_) + bucket_count_ * sizeof(bucket_t)) {
      throw std::invalid_argument("invalid flat hash data");
    }

    if (std::uintptr_t(read_ptr) % alignof(bucket_t) != 0) {
      throw std::invalid_argument("flat hash data appears to be misaligned");
    }

    static_assert(std::is_trivially_constructible_v<bucket_t>);
    buckets_ = new (read_ptr) bucket_t[bucket_count_];

    std::uint32_t header_mem_size =
        sizeof(bucket_count_) + bucket_count_ * sizeof(bucket_t);
    constexpr auto elem_align = alignof(elem_t);
    static_assert((elem_align & (elem_align - 1)) == 0);
    header_mem_size = (header_mem_size + (elem_align - 1)) & ~(elem_align - 1);

    read_ptr = mem_loc + header_mem_size;
    if (std::uintptr_t(read_ptr) % alignof(elem_t) != 0) {
      throw std::invalid_argument("flat hash data appears to be misaligned");
    }

    elem_count_ = 0;
    for (std::uint32_t i = 0; i < bucket_count_; ++i) {
      if (buckets_[i].offset > elem_count_) {
        throw std::invalid_argument("invalid flat hash data");
      }

      elem_count_ += buckets_[i].count;
    }

    static_assert(std::is_trivially_constructible_v<elem_t>);
    elems_ = new (read_ptr) elem_t[elem_count_];
  }

  iterator begin() const { return elems_; }

  iterator end() const { return elems_ + elem_count_; }

  ValT& at_key(HashT const& key_hash) {
    auto const& bucket = buckets_[key_hash % bucket_count_];

    if (bucket.count > 0) {
      elem_t* elem_table = elems_ + bucket.offset;

      auto end = elem_table + bucket.count;

      // Elements within a bucket are stored as a sorted vector
      auto found = std::lower_bound(
          elem_table, end, key_hash,
          [](elem_t const& lhs, HashT const& rhs) { return lhs.first < rhs; });

      if (found != end && found->first == key_hash) {
        return found->second;
      }
    }

    throw std::runtime_error("element not present in flat hash table");
  }

  template <typename KeyT>
  ValT& at(KeyT const& key) {
    HashT key_hash = std::hash<KeyT>{}(key);

    return at_key(key_hash);
  }

  template <typename KeyT>
  ValT const& at(KeyT const& key) const {
    return const_cast<Flat_hash_table*>(this)->at(key);
  }

 private:
  char* mem_loc_;

  struct bucket_t {
    std::uint32_t count;
    std::uint32_t offset;
  };

  std::uint32_t bucket_count_;
  bucket_t const* buckets_;

  std::uint32_t elem_count_;
  elem_t* elems_;

  template <typename V, typename H>
  friend std::vector<char> bake_flat_hash_table(
      std::vector<std::pair<H, V>> const&);
};

// Bakes a dataset into a flat_has_table raw data chunk.
template <typename ValT, typename HashT>
std::vector<char> bake_flat_hash_table(
    std::vector<std::pair<HashT, ValT>> const& data) {
  using table_t = Flat_hash_table<ValT, HashT>;
  using elem_t = typename table_t::elem_t;
  using bucket_t = typename table_t::bucket_t;

  static_assert(std::is_trivial_v<ValT>);
  static_assert(std::is_trivial_v<HashT>);

  // TODO: Better process to determine optimal bucket count.
  std::uint32_t bucket_count = std::uint32_t(data.size() * 2);
  std::vector<std::vector<elem_t>> buckets(bucket_count);

  {
    // Keep track of seen hashes since we do not tolerate true collisions
    std::unordered_set<HashT> hash_values_set;
    for (auto const& d : data) {
      HashT hash_val = d.first;

      if (hash_values_set.count(hash_val) != 0) {
        throw std::runtime_error(
            "True hash collision in dataset, cannot make a flat hash table out "
            "of it.");
      }
      hash_values_set.insert(hash_val);

      buckets[hash_val % bucket_count].emplace_back(elem_t{hash_val, d.second});
    }
  }

  std::size_t header_mem_size = 0;
  header_mem_size += sizeof(std::uint32_t);            // for bucket_count
  header_mem_size += sizeof(bucket_t) * bucket_count;  // bucket table

  // Make sure the actual value payloads is correctly aligned
  constexpr auto elem_align = alignof(elem_t);
  static_assert((elem_align & (elem_align - 1)) == 0);
  header_mem_size = (header_mem_size + (elem_align - 1)) & ~(elem_align - 1);

  auto mem_size = header_mem_size + sizeof(elem_t) * data.size();

  std::vector<char> result(mem_size);

  char* header_w_ptr = result.data();
  char* data_w_ptr = result.data() + header_mem_size;

  auto write = [&](char*& dst, auto const& v) {
    assert(dst + sizeof(v) <= result.data() + result.size());
    std::memcpy(dst, &v, sizeof(v));
    dst += sizeof(v);
  };

  write(header_w_ptr, bucket_count);

  std::uint32_t total_count = 0;
  for (auto& b : buckets) {
    std::sort(b.begin(), b.end(), [](auto const& lhs, auto const& rhs) {
      return lhs.first < rhs.first;
    });

    auto offset = data_w_ptr - result.data();
    bucket_t bucket_header{std::uint32_t(b.size()), total_count};
    total_count += std::uint32_t(b.size());

    write(header_w_ptr, bucket_header);

    for (auto const& e : b) {
      write(data_w_ptr, e);
    }
  }
  return result;
}
}  // namespace slt
#endif