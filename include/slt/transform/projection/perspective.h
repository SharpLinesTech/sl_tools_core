#ifndef SLT_TRANSFORMS_PROJECTION_PERSPECTIVE_H_INCLUDED
#define SLT_TRANSFORMS_PROJECTION_PERSPECTIVE_H_INCLUDED

#include "slt/math/angle/angle.h"
#include "slt/math/angle/operations.h"
#include "slt/math/vec.h"

namespace slt {
template <typename T>
struct Perspective {
 public:
  Perspective(const Angle<T>& vfov, T aspect_ratio, const Vec2<T>& depth_range)
      : vfov_(vfov), aspect_(aspect_ratio), z_range_(depth_range) {}

  Vec3<T> transform(const Vec3<T>& v) {
    Vec4<T> as_vec4{v[0], v[1], v[2], T(1)};

    as_vec4 = Mat4<T>(*this) * as_vec4;
    T w_inv = T(1) / as_vec4[3];

    return {as_vec4[0] * w_inv, as_vec4[1] * w_inv, as_vec4[2] * w_inv};
  }

  operator Mat4<T>() const {
    Mat4<T> result = {0};

    auto f = T(1) / tan(vfov_ * 0.5f);
    auto near_f = z_range_[0];
    auto far_f = z_range_[1];

    result(0, 0) = f / aspect_;
    result(1, 1) = f;
    result(2, 2) = (far_f + near_f) / (near_f - far_f);
    result(2, 3) = -T(1);
    result(3, 2) = T(2) * far_f * near_f / (near_f - far_f);

    return result;
  }

 private:
  Angle<T> vfov_;
  T aspect_;
  Vec2<T> z_range_;
};

}  // namespace slt

#endif