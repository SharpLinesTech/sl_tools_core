#ifndef SLT_TRANSFORM_TRANSLATE_H
#define SLT_TRANSFORM_TRANSLATE_H

#include "slt/math/vec.h"

namespace slt {

template <typename T>
struct Translate {
  Translate(const Vec<T, 3>& t) : t_(t) {}

  Vec<T, 3> transform(const Vec<T, 3>& v) const { return v + t_; }

  operator Mat<T, 4, 4>() const {
    return {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, t_[0], t_[1], t_[2], 1};
  }

  Vec<T, 3> t_;
};

template <typename T>
inline Vec<T, 3> operator*(const Translate<T>& t, Vec<T, 3> const& v) {
  return t.transform(v);
}

template <typename T>
Translate(const Vec<T, 3>&)->Translate<T>;
}  // namespace slt

#endif