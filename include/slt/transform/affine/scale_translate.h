#ifndef SLT_TRANSFORM_SCALE_TRANSLATE_H
#define SLT_TRANSFORM_SCALE_TRANSLATE_H

#include "slt/math/vec.h"

#include "slt/transform/affine/scale.h"
#include "slt/transform/affine/translate.h"

namespace slt {

template <typename T>
struct Scale_translate {
  Scale_translate(const Vec<T, 3>& s, const Vec<T, 3>& t) : s_(s), t_(t) {}

  Vec<T, 3> transform(const Vec<T, 3>& v) const { return v * s_ + t_; }

  operator Mat<T, 4, 4>() const {
    return {s_[0], 0, 0,     0, 0,     s_[1], 0,     0,
            0,     0, s_[2], 0, t_[0], t_[1], t_[2], 1};
  }

  Vec<T, 3> s_;
  Vec<T, 3> t_;
};

template <typename T>
inline Vec<T, 3> operator*(const Scale<T>& t, Vec<T, 3> const& v) {
  return t.transform(v);
}

template <typename T>
inline Scale_translate<T> operator*(const Scale<T>& s, Translate<T> const& t) {
  return Scale_translate<T>(s.s_, t.t_ * s.s_);
}

template <typename T>
inline Scale_translate<T> operator*(Translate<T> const& t, const Scale<T>& s) {
  return Scale_translate<T>(s.s_, t.t_);
}

template <typename T>
Scale_translate(const Vec<T, 3>&, const Vec<T, 3>&)->Scale_translate<T>;
}  // namespace slt

#endif