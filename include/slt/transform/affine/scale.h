#ifndef SLT_TRANSFORM_SCALE_H
#define SLT_TRANSFORM_SCALE_H

#include "slt/math/vec.h"

namespace slt {

template <typename T>
struct Scale {
  Scale(const Vec<T, 3>& s) : s_(s) {}

  Vec<T, 3> transform(const Vec<T, 3>& v) const { return v * s_; }

  operator Mat<T, 4, 4>() const {
    return {s_[0], 0, 0, 0, 0, s_[1], 0, 0, 0, 0, s_[2], 0, 0, 0, 0, 1};
  }

  Vec<T, 3> s_;
};

template <typename T>
inline Vec<T, 3> operator*(const Scale<T>& t, Vec<T, 3> const& v) {
  return t.transform(v);
}

template <typename T>
Scale(const Vec<T, 3>&)->Scale<T>;
}  // namespace slt

#endif