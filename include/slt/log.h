#ifndef SLT_LOG_LOG_H
#define SLT_LOG_LOG_H

#include "slt/settings.h"

#ifdef WIN32
#pragma warning(disable : 4244)
#endif

#define FMT_THROW(x) assert(false)

#include "spdlog/fmt/ostr.h"
#include "spdlog/spdlog.h"

#ifdef WIN32
#pragma warning(default : 4244)
#endif

// spdlog includes windows.h, which conflicts with capnp
#ifdef WIN32
#undef ERROR
#undef VOID
#endif

namespace slt {

// The global log instance. See the declaration of spdlog::logger for usage.
extern std::shared_ptr<spdlog::logger> log;

namespace settings {
extern Setting<bool> log_async;
extern Setting<int32_t> async_log_queue;
}  // namespace settings
}  // namespace slt

#endif
