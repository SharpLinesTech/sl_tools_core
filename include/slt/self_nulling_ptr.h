#ifndef SLT_SELF_NULLING_PTR_INCLUDED_H
#define SLT_SELF_NULLING_PTR_INCLUDED_H

#include <atomic>

#ifndef NDEBUG
#include <set>
#include <thread>
#endif

#include "slt/debug/assert.h"

namespace slt {

template <typename T>
struct self_nulling_ptr_storage_ {
  T* ptr;
  std::atomic<int> count{1};

#ifndef NDEBUG
  std::set<std::thread::id> getting_threads;
#endif
};

template <typename T>
class self_nulling_ptr {
 public:
  self_nulling_ptr(self_nulling_ptr const& rhs) : storage_(rhs.storage_) {
    if (storage_) {
      storage_->count++;
    }
  }

  self_nulling_ptr(self_nulling_ptr&& rhs) : storage_(rhs.storage_) {
    rhs.storage_ = nullptr;
  }

  self_nulling_ptr& operator=(self_nulling_ptr const& rhs) {
    storage_ = rhs.storage_;
    if (storage_) {
      storage_->count++;
    }
  }

  self_nulling_ptr& operator=(self_nulling_ptr&& rhs) {
    storage_ = rhs.storage_;
    rhs.storage_ = nullptr;
  }

  ~self_nulling_ptr() {
    if (storage_) {
      // Despite appearances, this should be safe. Self-nulling pointers can
      // only be created from either the original object, or another
      // self-nulling pointer that is keeping the count >= 1.
      if (storage_->count-- == 0) {
        delete storage_;
      }
    }
  }

  T* get() const {
#ifndef NDEBUG
    storage_->getting_threads.insert(std::this_thread::get_id());
#endif
    return storage_->ptr;
  }

  operator bool() const { return get() != nullptr; }
  T* operator->() const { return get(); }
  T& operator*() const { return *get(); }

 private:
  template <typename CRTP>
  friend class enable_self_nulling_ptr;

  self_nulling_ptr(self_nulling_ptr_storage_<T>* storage) : storage_(storage) {
    storage->count++;
  }
  self_nulling_ptr_storage_<T>* storage_;
};

template <typename CRTP>
class enable_self_nulling_ptr {
 public:
  enable_self_nulling_ptr() = default;

  // Returns a self-nulling pointer to this object.
  self_nulling_ptr<CRTP> get_self_nulling_ptr() {
    if (!weak_data_) {
      weak_data_ = new self_nulling_ptr_storage_<CRTP>;
      weak_data_->ptr = static_cast<CRTP*>(this);
    }

    return self_nulling_ptr<CRTP>(weak_data_);
  }

  // explicitely do nothing:
  // N.B. It could be possible to handle move construction/assignment
  // but doing so in a thread-safe manner would be very messy and
  // probably not worth the trouble.
  enable_self_nulling_ptr(enable_self_nulling_ptr&& rhs) {}
  enable_self_nulling_ptr& operator=(enable_self_nulling_ptr&& rhs) {}
  enable_self_nulling_ptr(enable_self_nulling_ptr const& rhs) {}
  enable_self_nulling_ptr& operator=(enable_self_nulling_ptr const&) {}

  // On destruction,
  // If there are nullable pointers in existence, set the storage to null
  // Otherwise delete the storage.
  ~enable_self_nulling_ptr() {
    if (weak_data_) {
#ifndef NDEBUG
      auto this_id = std::this_thread::get_id();
      for (auto i : weak_data_->getting_threads) {
        SLT_ASSERT(i == this_id);
      }
#endif
      if (weak_data_->count-- == 0) {
        delete weak_data_;
      } else {
        weak_data_->ptr = nullptr;
      }
    }
  }

 private:
  self_nulling_ptr_storage_<CRTP>* weak_data_ = nullptr;
};
}  // namespace slt
#endif
