#ifndef SLT_SETTINGS_SETTINGS_H
#define SLT_SETTINGS_SETTINGS_H

#include <functional>
#include <iostream>
#include <string>
#include <string_view>

#include <cstdlib>
#include <memory>
#include <sstream>

#include "slt/debug/assert.h"

#ifdef __GNUG__
#include <cxxabi.h>
namespace {
template <typename T>
std::string get_type_name() {
  int status = 0;
  auto name = abi::__cxa_demangle(typeid(T).name(), nullptr, 0, &status);

  std::string result = name;
  free(name);
  return result;
}
}  // namespace
#else

namespace {
template <typename T>
std::string get_type_name() {
  return typeid(T).name();
}
}  // namespace
#endif

namespace slt {
// Error Thrown when something goes wrong will seting up or processing Settings.
struct Settings_error : public std::runtime_error {
  Settings_error(std::string const& err) : std::runtime_error(err) {}
};

class Setting_base {
 public:
  Setting_base(std::string name, std::string description)
      : name_(std::move(name)), description_(std::move(description)) {}

  virtual ~Setting_base() {}

  std::string const& description() const { return description_; }
  std::string const& name() const { return name_; }

  virtual std::string help_string() const = 0;
  virtual void set_from_string(std::string_view val) = 0;
  virtual void reset() = 0;

 protected:
  static void registerSetting(Setting_base*);

 private:
  std::string name_;
  std::string description_;
};

namespace detail {
template <typename T>
struct Setting_value_assign {
  static T assign(std::string_view val) {
    T result;
    std::string as_string(val);
    std::istringstream stream(as_string);
    stream >> result;
    return result;
  }
};

template <typename T>
struct Setting_value_assign<std::vector<T>> {
  static std::vector<T> assign(std::string_view val) {
    std::vector<T> result;
    SLT_UNIMPLEMENTED();
    return result;
  }
};

template <>
struct Setting_value_assign<bool> {
  static bool assign(std::string_view val) {
    return val == "true" || val == "1";
  }
};

template <typename Setting_T>
struct Setting_help_string {
  using value_type = typename Setting_T::value_type;

  static std::string get(const Setting_T& setting) {
    return setting.name() + "=" + get_type_name<value_type>() + " : " +
           setting.description();
  }
};

}  // namespace detail

// Settings are self-registering global properties that will be set when the slt
// Core is being initialized.
//
// Settings should always be defined as global variables.
// Settings will have their default until the core has been initialized, and
// after the core has shut down.
template <typename T>
class[[nodiscard]] Setting : public Setting_base {
 public:
  using value_type = T;

  using Validator = std::function<bool(T const&)>;

  Setting(std::string name, T default_val, std::string description,
          Validator validator = nullptr)
      : Setting_base(std::move(name), std::move(description)),
        default_(std::move(default_val)),
        value_(default_),
        validator_(validator) {
    if (validator_ && !validator_(default_val)) {
      throw Settings_error("Invalid default value");
    }

    registerSetting(this);
  }

  // Retrieve the value of the setting.
  T const& get() const { return value_; }

  // Sets the value of the setting.
  void set(T const& v) {
    if (validator_ && !validator_(v)) {
      throw Settings_error("attempting to set invalid value");
    }
    value_ = v;
  }

  void set_from_string(std::string_view val) override {
    value_ = detail::Setting_value_assign<T>::assign(val);
  }

  std::string help_string() const override {
    return detail::Setting_help_string<Setting<T>>::get(*this);
  }

  // Resets the setting to whatever its default is.
  void reset() override { value_ = default_; }

  // Changes the default value of the setting. Be careful, this change does NOT
  // revert when resetting the setting.
  void setDefault(T const& new_val) {
    if (validator_ && !validator_(new_val)) {
      throw Settings_error("attempting to set invalid default value");
    }
    default_ = new_val;
    value_ = new_val;
  }

 private:
  T default_;
  T value_;
  Validator validator_;
};
}  // namespace slt

#endif
