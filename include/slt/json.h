#ifndef SLT_JSON_H
#define SLT_JSON_H

#include "nlohmann/json.hpp"

namespace slt {
using json = nlohmann::json;
}
#endif