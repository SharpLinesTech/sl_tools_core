#ifndef SLT_COLOR_H
#define SLT_COLOR_H

#include "slt/math/vec.h"

namespace slt {
using Color = fVec4;

constexpr Color red = {1.0f, 0.0f, 0.0f, 1.0f};
constexpr Color green = {0.0f, 1.0f, 0.0f, 1.0f};
constexpr Color blue = {0.0f, 0.0f, 1.0f, 1.0f};

constexpr Color yellow = {1.0f, 1.0f, 0.0f, 1.0f};

constexpr Color white = {1.0f, 1.0f, 1.0f, 1.0f};
constexpr Color black = {0.0f, 0.0f, 0.0f, 1.0f};
}  // namespace slt

#endif