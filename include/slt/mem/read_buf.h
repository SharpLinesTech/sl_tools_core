#ifndef SLT_MEMORY_READ_BUF_H
#define SLT_MEMORY_READ_BUF_H

#include <streambuf>
#include <string_view>

namespace slt {

struct Read_buf : public std::streambuf {
  Read_buf(std::string_view str) {
    char* p = const_cast<char*>(str.data());
    this->setg(p, p, p + str.length());
  }
  Read_buf(char const* start, std::size_t size) {
    char* p = const_cast<char*>(start);
    this->setg(p, p, p + size);
  }
};
}  // namespace slt
#endif