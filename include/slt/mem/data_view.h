#ifndef SLT_MEMORY_DATA_VIEW_H
#define SLT_MEMORY_DATA_VIEW_H

#include <memory>
#include <string>
#include <type_traits>
#include <vector>
#include "slt/debug/assert.h"
#include "slt/log.h"
#include "slt/mem/alignment.h"
#include "slt/mem/data_block.h"
//#include <capnp/message.h>

namespace slt {

struct Data_view {
  Data_view(char* d, std::size_t s) : data(d), size(s) {}

  Data_view(Data_block& d) : data(d.data()), size(d.size()) {}

  Data_view(std::string& d) : data(d.data()), size(d.length()) {}

  explicit operator Data_block() const { return Data_block(data, data + size); }

  char* data;
  std::size_t size;
};

struct Const_data_view {
  Const_data_view() : data(nullptr), size(0) {}

  Const_data_view(const char* d, std::size_t s) : data(d), size(s) {}

  Const_data_view(const Data_block& d) : data(d.data()), size(d.size()) {}

  Const_data_view(const std::string& d) : data(d.data()), size(d.length()) {}

  Const_data_view(Data_view d) : data(d.data), size(d.size) {}

  const char* data;
  std::size_t size;
};

}  // namespace slt

#endif
