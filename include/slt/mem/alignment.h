#ifndef SLT_MEM_MISC_H
#define SLT_MEM_MISC_H

#include <cstdint>
#include <memory>
#include "slt/debug/invariant.h"

namespace slt {
namespace mem {

struct Size_and_align {
  std::size_t size;
  std::size_t align;
};

// Tests wether a pointer is aligned to a given memory boundary.
inline bool is_aligned(void const* ptr, std::size_t align) {
  return (std::uintptr_t(ptr) % align) == 0;
}

inline std::size_t align_size(std::size_t size, std::size_t align) {
  // make sure we are dealing with a power of two
  SLT_ASSERT((align & (align - 1)) == 0);
  return (size + (align - 1)) & ~(align - 1);
}
}  // namespace mem

template <>
inline void check_invariant<mem::Size_and_align>(mem::Size_and_align const& v) {
  SLT_ASSERT_GT(v.align, 0);
}
}  // namespace slt
#endif