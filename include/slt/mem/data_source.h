#ifndef SLT_MEMORY_DATA_SOURCE_H
#define SLT_MEMORY_DATA_SOURCE_H

#include "slt/mem/data_block.h"
#include "slt/mem/data_view.h"

#include <variant>

namespace slt {

// A Data_source is used when we want a data consumer to be compatible
// with different models of data ownership.
class Data_source {
 public:
  Data_source(Const_data_view v) : data_(v) {}

  Data_source(Data_view v) : data_(Const_data_view(v)) {}

  Data_source(Data_block d) : data_(std::move(d)) {}

  Const_data_view data() const {
    Const_data_view result;

    std::visit([&result](const auto& d) { result = d; }, data_);

    return result;
  }

 private:
  std::variant<Const_data_view, Data_block> data_;
};

}  // namespace slt
#endif