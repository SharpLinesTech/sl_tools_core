#ifndef SLT_RECT_PACK_H
#define SLT_RECT_PACK_H

#include "slt/math/vec.h"

#include <algorithm>
#include <cassert>
#include <limits>
#include <optional>
#include <tuple>
#include <vector>

namespace slt {

// Default scorer: fits the rect as snuggly as possible in a dimension
// Note: lower means better.
inline int default_rect_score(const iVec2& src, const iVec2& dst) {
  return std::max(dst[0] - src[0], dst[1] - src[1]);
}

inline bool rect_fits(const iVec2& src_size, const iVec2& dst_size) {
  return dst_size[0] >= src_size[0] && dst_size[1] >= src_size[1];
}

struct Packing_free_rect {
  iVec2 pos;
  iVec2 size;

  // Two rects with bi-directional linking have an overlapping
  // section following this pattern:
  // XXXAAA
  // XXXAAA
  // BBB***
  // (the * part belongs to both A and B).
  // Whichever rect, A or B, is used first will claim the shared section as
  // its own, and it's counterpart will be shrunk accordingly.
  int counterpart = -1;
};

struct Rect_packing_result {
  iVec2 pos;
  bool flipped;
};

// This is a simple incremental rect packer.
//
// For optimal results, you should sort the list of rectangles to pack
// beforehand, but this is not a hard requirement.
//
// If using a custom allocator, it has to be rebindable.
template <typename Alloc = std::allocator<char>>
class Rect_packer_2D {
 public:
  explicit Rect_packer_2D(iVec2 bin_size, const Alloc& alloc = Alloc());

  //
  template <typename Scorer>
  std::optional<Rect_packing_result> pack(iVec2 rect, Scorer scorer);

  // If calling this, you are responsible for ensure it does not overlap with
  // any other existing free rect. returns wether insertion was successful.
  bool add_free_rect(const iVec2& pos, const iVec2& size);

 private:
  using Free_rect_allocator = typename std::allocator_traits<
      Alloc>::template rebind_alloc<Packing_free_rect>;
  using Free_rect_collection =
      std::vector<Packing_free_rect, Free_rect_allocator>;
  using Free_rect_ite = typename Free_rect_collection::iterator;

  template <typename RectScorer>
  std::tuple<Free_rect_ite, bool> choose_slot_(const iVec2& rect,
                                               RectScorer scorer) {
    const int non_fit_score = std::numeric_limits<int>::max();

    iVec2 flipped_rect = {rect[1], rect[0]};

    // 1. Identify where we will store the rect.
    int best_score = non_fit_score;
    auto best_rect = free_rects_.end();
    bool flip = false;

    for (auto ite = free_rects_.begin(); ite != free_rects_.end(); ++ite) {
      // Test for perfect fit
      if (rect == ite->size) {
        best_rect = ite;
        flip = false;
        break;
      }

      if (flipped_rect == ite->size) {
        best_rect = ite;
        flip = true;
        break;
      }

      auto score =
          rect_fits(rect, ite->size) ? scorer(rect, ite->size) : non_fit_score;
      auto flipped_score = rect_fits(flipped_rect, ite->size)
                               ? scorer(flipped_rect, ite->size)
                               : non_fit_score;

      if (score < best_score) {
        best_rect = ite;
        best_score = score;
        flip = false;
      }

      if (flipped_score < best_score) {
        best_rect = ite;
        best_score = score;
        flip = true;
      }
    }

    return std::make_tuple(best_rect, flip);
  }

  void remove_free_rect_(Free_rect_ite target) {
    if (target->counterpart != -1) {
      // If the chosen rect had a counterpart, shrink it.
      auto cp = &free_rects_[target->counterpart];
      assert(cp->counterpart == target - free_rects_.begin());

      if (cp->pos[0] < target->pos[0]) {
        // cp was the bottom one
        cp->size[0] = target->pos[0] - cp->pos[0];
      } else {
        // cp was the top one
        assert(cp->pos[1] < target->pos[1]);
        cp->size[1] = target->pos[1] - cp->pos[1];
      }
      cp->counterpart = -1;
    }

    // If appropriate, move the rect that was at the end of the list
    // to the position the removed rect used to occupy.
    auto last = std::prev(free_rects_.end());
    if (last != target) {
      *target = std::move(*last);
      if (target->counterpart != -1) {
        auto cp = &free_rects_[target->counterpart];
        cp->counterpart = int(target - free_rects_.begin());
      }
    }
    free_rects_.pop_back();
  }

  Free_rect_collection free_rects_;
};

template <typename Alloc>
Rect_packer_2D<Alloc>::Rect_packer_2D(iVec2 bin_size, const Alloc& alloc)
    : free_rects_(alloc) {
  add_free_rect({0, 0}, bin_size);
}

template <typename Alloc>
template <typename Scorer>
std::optional<Rect_packing_result> Rect_packer_2D<Alloc>::pack(iVec2 rect,
                                                               Scorer scorer) {
  // 1. Chose where to store the rect
  auto [chosen_slot, flipped] = choose_slot_(rect, scorer);

  if (chosen_slot == free_rects_.end()) {
    return std::nullopt;
  }

  auto destination = *chosen_slot;
  if (flipped) {
    std::swap(rect[0], rect[1]);
  }

  // 2. remove the chosen rect from the free list
  remove_free_rect_(chosen_slot);
  // N.B. chosen_slot is an invalid iterator past this spot

  // 3. add the remainders to the free list
  const auto rem_w = destination.size[0] - rect[0];
  const auto rem_h = destination.size[1] - rect[1];

  bool right_added =
      add_free_rect({destination.pos[0] + rect[0], destination.pos[1]},
                    {rem_w, destination.size[1]});

  bool bottom_added =
      add_free_rect({destination.pos[0], destination.pos[1] + rect[1]},
                    {destination.size[0], rem_h});

  // If both insertions were successful, bind them together.
  if (right_added && bottom_added) {
    std::prev(free_rects_.end())->counterpart = int(free_rects_.size() - 2);
    std::prev(free_rects_.end(), 2)->counterpart = int(free_rects_.size() - 1);
  }

  return Rect_packing_result{destination.pos, flipped};
}

template <typename Alloc>
bool Rect_packer_2D<Alloc>::add_free_rect(const iVec2& pos, const iVec2& size) {
  if (size[0] > 0 && size[1] > 0) {
    free_rects_.emplace_back(Packing_free_rect{pos, size});
    return true;
  }
  return false;
}
}  // namespace slt
#endif
