#ifndef SLT_SCOPE_GUARD_H
#define SLT_SCOPE_GUARD_H

namespace slt {
template <typename T>
struct [[nodiscard]] scope_guard_t {
  scope_guard_t(T act) : action_(std::move(act)) {}

  scope_guard_t(scope_guard_t const &) = delete;
  scope_guard_t &operator=(scope_guard_t const &) = delete;

  scope_guard_t(scope_guard_t && rhs)
      : action_(std::move(rhs.action_)), dismissed_(rhs.dismissed_) {
    rhs.dismissed_ = true;
  }

  scope_guard_t &operator=(scope_guard_t &&rhs) {
    action_ = std::move(rhs.action_);
    dismissed_ = rhs.dismissed_;
    rhs.dismissed_ = true;
  }

  ~scope_guard_t() {
    if (!dismissed_) {
      try {
        action_();
      } catch (...) {
      }
    }
  }

  void dismiss() { dismissed_ = true; }

 private:
  T action_;
  bool dismissed_ = false;
};

template <typename T>
scope_guard_t<T> on_scope_exit(T act) {
  return scope_guard_t<T>(std::move(act));
}

}  // namespace slt

#endif
