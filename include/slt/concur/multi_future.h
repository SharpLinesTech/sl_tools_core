#ifndef SLT_MULTI_FUTURE_H
#define SLT_MULTI_FUTURE_H

#include "slt/concur/future.h"
#include "slt/tuple_support.h"

namespace slt {

  template<typename T>
  struct MF_Completion;

  template<typename T>
  struct MF_Completion<Future<T>> {
    using completion_type = T;
  };

  template<typename T>
  struct MF_Completion<Shared_future<T>> {
    using completion_type = Shared_future<T>;
  };

  template<typename Dst_store_t, typename F_T, typename... ChildsT>
  struct Multi_future_storage : public std::enable_shared_from_this<Multi_future_storage<Dst_store_t, F_T, ChildsT...>>{
    static constexpr std::size_t count = sizeof...(ChildsT);
    using value_type = typename Dst_store_t::value_type;

    Multi_future_storage(concur::Work_pool* pool, std::shared_ptr<Dst_store_t> dst, F_T cb) : ann_(pool->announce_job()), dst_store_(std::move(dst)), cb_(std::move(cb)) {}

    std::shared_ptr<Dst_store_t> dst_store_;
    F_T cb_;
    concur::Announced_job ann_;

    std::atomic<std::size_t> completed_ = 0;

    std::mutex mutex_;    
    std::tuple<typename MF_Completion<ChildsT>::completion_type...> completions_;

    void child_complete() {
      auto c = completed_ += 1;
      if(c == count) {
        auto self = this->shared_from_this();
        ann_.push([this, self]{
          if constexpr(std::is_same_v<value_type, void>) {
            std::apply(cb_, std::move(completions_));
            dst_store_->fullfill();
          }
          else {
            dst_store_->fullfill(std::apply(cb_, std::move(completions_)));
          }
        });
      }
    }
  };

  template<typename... ChildsT>
  struct Multi_future_arg {
    using args_type = std::tuple<>;
  };

  template<typename FrontT, typename... RestT>
  struct Multi_future_arg<Future<FrontT>, RestT...> {
    using args_type = std::conditional_t<std::is_same_v<void, FrontT>,
      typename Multi_future_arg<RestT...>::args_type,
      typename tuple_prepend<FrontT, typename Multi_future_arg<RestT...>::args_type>::type
    >;
  };

  template<typename FrontT, typename... RestT>
  struct Multi_future_arg<Shared_future<FrontT>, RestT...> {
    using args_type = std::conditional_t<std::is_same_v<void, FrontT>,
      typename Multi_future_arg<RestT...>::args_type,
      typename tuple_prepend<Shared_future<FrontT>, typename Multi_future_arg<RestT...>::args_type>::type
    >;
  };


  template<typename F_T, typename Args>
  struct Multi_future_result;

  template<typename F_T, typename... Args>
  struct Multi_future_result<F_T, std::tuple<Args...>> {
    using type = std::invoke_result_t<F_T, Args...>;
  };

//  template<typenam>

  template<std::size_t id, typename StorageT, typename... ChildsT>
  void handle_multi_then_childs(std::shared_ptr<StorageT> storage, std::tuple<ChildsT...>& childs) {
    constexpr auto tup_size = sizeof...(ChildsT);

    if constexpr(id < tup_size) {
      auto& f = std::get<id>(childs);
      if constexpr(is_shared_future<std::decay_t<decltype(f)>>::value) {
        f.then([storage, f](auto const&) {
          //We store the shared future itself, since it's what's managing the lifetime
          std::get<id>(storage->completions_) = std::move(f);
          storage->child_complete();
        });
      }
      else {
        f.then([storage](auto v) {
          std::get<id>(storage->completions_) = std::move(v);
          storage->child_complete();
        });
      }


      handle_multi_then_childs<id+1>(std::move(storage), childs);
    }
  }

  template<typename... ChildsT>
  struct Tied_futures {
    Tied_futures(ChildsT... childs) : childs_(std::move(childs)...) {}

    template<typename F_T>
    auto then(F_T fn, concur::Work_pool* dst = concur::Work_pool::current()) {
      using args = typename Multi_future_arg<ChildsT...>::args_type;

      using func_result_t = typename Multi_future_result<F_T, args>::type;
      using sel_t = Future_storage_selector<func_result_t>;

      using store_t = typename sel_t::store_type;
      using fut_t = typename sel_t::future_type;

      auto res_fs = std::make_shared<store_t>();


      using arg_storage_t = Multi_future_storage<store_t, F_T, ChildsT...>;
      auto state = std::make_shared<arg_storage_t>(dst, res_fs, std::move(fn));

      handle_multi_then_childs<0>(std::move(state), childs_);

      return fut_t(res_fs);
    }

    std::tuple<ChildsT...> childs_;
  };

  template<typename... Fut_t>
  auto tie_futures(Fut_t... child) {
    return Tied_futures<Fut_t...>(std::move(child)...);
  };

}
#endif