#ifndef SLT_FUTURE_H
#define SLT_FUTURE_H

#include <cassert>
#include <functional>
#include <memory>
#include <mutex>
#include <variant>

#include "slt/concur/work_pool.h"

namespace slt {

template <typename T>
class Future;

template <typename T>
class Shared_future;

template <typename T>
class Promise;

// ****************** PROMISED ****************** //

enum class Future_State {
  pending,
  fullfilled,
  errored,
  invalid,
};

template <typename T>
class Future_storage {
 public:
  using value_type = T;
  using on_ready_t = std::function<void(T)>;
  using on_error_t = std::function<void(std::exception_ptr)>;

  struct pending_t {
    on_ready_t on_ready_;
    on_error_t on_error_;
  };

  void fullfill(Future<T> value) {
    std::lock_guard l(value.store_->mutex_);

    switch (value.store_->state()) {
      case Future_State::fullfilled:
        fullfill(value.store_->data());
        break;
      case Future_State::errored:
        fullfill(value.store_->exception());
        break;
      case Future_State::pending:
        value.store_->set_handlers(
            [this](T v) { fullfill(std::move(v)); },
            [this](std::exception_ptr e) { fullfill(std::move(e)); });
        break;
    }
  }

  void fullfill(T value) {
    std::lock_guard l(mutex_);
    assert(state() == Future_State::pending);

    auto& p = std::get<pending_t>(data_);

    if (p.on_ready_) {
      p.on_ready_(std::move(value));
    } else {
      data_ = std::move(value);
    }
  }

  void fullfill(std::exception_ptr error) {
    std::lock_guard l(mutex_);
    assert(state() == Future_State::pending);

    auto& p = std::get<pending_t>(data_);

    if (p.on_error_) {
      p.on_error_(std::move(error));
    } else {
      data_ = std::move(error);
    }
  }

  Future_State state() const {
    switch (data_.index()) {
      case 0:
        return Future_State::pending;
      case 1:
        return Future_State::fullfilled;
      case 2:
        return Future_State::errored;
      default:
        break;
    }
    return Future_State::invalid;
  }

  T data() { return std::move(std::get<T>(data_)); }

  std::exception_ptr exception() {
    return std::move(std::get<std::exception_ptr>(data_));
  }

  void set_handlers(on_ready_t r, on_error_t e) {
    assert(r && e);
    assert(state() == Future_State::pending);

    auto& p = std::get<pending_t>(data_);
    assert(!p.on_ready_ && !p.on_error_);

    p.on_ready_ = std::move(r);
    p.on_error_ = std::move(e);
  }

  std::mutex mutex_;
  std::variant<pending_t, T, std::exception_ptr> data_;
};

template <>
class Future_storage<void> {
 public:
  using value_type = void;
  using on_ready_t = std::function<void()>;
  using on_error_t = std::function<void(std::exception_ptr)>;

  struct pending_t {
    on_ready_t on_ready_;
    on_error_t on_error_;
  };

  void fullfill() {
    std::lock_guard l(mutex_);
    assert(state() == Future_State::pending);

    auto& p = std::get<pending_t>(data_);

    if (p.on_ready_) {
      p.on_ready_();
    } else {
      data_ = true;
    }
  }

  void fullfill(std::exception_ptr error) {
    std::lock_guard l(mutex_);
    assert(state() == Future_State::pending);

    auto& p = std::get<pending_t>(data_);

    if (p.on_error_) {
      p.on_error_(std::move(error));
    } else {
      data_ = std::move(error);
    }
  }

  Future_State state() const {
    switch (data_.index()) {
      case 0:
        return Future_State::pending;
      case 1:
        return Future_State::fullfilled;
      case 2:
        return Future_State::errored;
      default:
        break;
    }
    return Future_State::invalid;
  }

  std::exception_ptr exception() {
    return std::move(std::get<std::exception_ptr>(data_));
  }

  void set_handlers(on_ready_t r, on_error_t e) {
    assert(r && e);
    assert(state() == Future_State::pending);

    auto& p = std::get<pending_t>(data_);
    assert(!p.on_ready_ && !p.on_error_);

    p.on_ready_ = std::move(r);
    p.on_error_ = std::move(e);
  }

  std::mutex mutex_;
  std::variant<pending_t, bool, std::exception_ptr> data_;
};

// ****************** PROMISE ****************** //
template <typename T>
class Promise {
 public:
  Promise() = default;
  Promise(Promise&&) = default;
  Promise(const Promise&) = delete;
  Promise& operator=(Promise&&) = default;
  Promise& operator=(const Promise&) = delete;

  Future<T> get_future() {
    assert(!store_);

    store_ = std::make_shared<Future_storage<T>>();
    return Future<T>(store_);
  }

  void set_value(T val) {
    assert(store_);
    store_->fullfill(std::move(val));
  }

  void set_exception(std::exception_ptr p) {
    assert(store_);
    store_->fullfill(std::move(p));
  }

 private:
  std::shared_ptr<Future_storage<T>> store_;
};

template <typename T>
class Shared_future_storage {
 public:
  using value_type = T;
  using on_ready_t = std::function<void(const T&)>;
  using on_error_t = std::function<void(std::exception_ptr)>;

  struct pending_t {
    std::vector<on_ready_t> on_ready_;
    std::vector<on_error_t> on_error_;
  };

  Shared_future_storage(std::shared_ptr<Future_storage<T>> parent)
      : parent_(std::move(parent)) {
    std::lock_guard l(parent_->mutex_);

    switch (parent_->state()) {
      case Future_State::pending:
        parent_->set_handlers(
            [this](T v) {
              std::lock_guard l(mutex_);

              assert(state() == Future_State::pending);
              auto p = std::move(std::get<pending_t>(data_));
              data_ = std::move(v);

              const auto& d = std::get<T>(data_);

              for (auto& cb : p.on_ready_) {
                cb(d);
              }
            },
            [this](std::exception_ptr e) {
              std::lock_guard l(mutex_);

              assert(state() == Future_State::pending);
              auto p = std::move(std::get<pending_t>(data_));
              data_ = std::move(e);

              auto& new_e = std::get<std::exception_ptr>(data_);

              for (auto& cb : p.on_error_) {
                cb(new_e);
              }
            });
        break;
      case Future_State::fullfilled:
        data_ = parent_->data();
        break;
      case Future_State::errored:
        data_ = parent_->exception();
      default:
        break;
    }
  }

  Future_State state() const {
    switch (data_.index()) {
      case 0:
        return Future_State::pending;
      case 1:
        return Future_State::fullfilled;
      case 2:
        return Future_State::errored;
      default:
        break;
    }
    return Future_State::invalid;
  }

  const T& data() { return std::get<T>(data_); }

  std::exception_ptr exception() {
    return std::move(std::get<std::exception_ptr>(data_));
  }

  void add_handlers(on_ready_t r, on_error_t e) {
    assert(r && e);
    assert(state() == Future_State::pending);

    auto& p = std::get<pending_t>(data_);

    p.on_ready_.emplace_back(std::move(r));
    p.on_error_.emplace_back(std::move(e));
  }

  std::mutex mutex_;

  std::variant<pending_t, T, std::exception_ptr> data_;

  std::shared_ptr<Future_storage<T>> parent_;
};

template <>
class Shared_future_storage<void> {
 public:
  using value_type = void;
  using on_ready_t = std::function<void()>;
  using on_error_t = std::function<void(std::exception_ptr)>;

  struct pending_t {
    std::vector<on_ready_t> on_ready_;
    std::vector<on_error_t> on_error_;
  };

  Shared_future_storage(std::shared_ptr<Future_storage<void>> parent)
      : parent_(std::move(parent)) {
    std::lock_guard l(parent_->mutex_);

    switch (parent_->state()) {
      case Future_State::pending:
        parent_->set_handlers(
            [this]() {
              std::lock_guard l(mutex_);

              assert(state() == Future_State::pending);
              auto p = std::move(std::get<pending_t>(data_));
              data_ = true;

              for (auto& cb : p.on_ready_) {
                cb();
              }
            },
            [this](std::exception_ptr e) {
              std::lock_guard l(mutex_);

              assert(state() == Future_State::pending);
              auto p = std::move(std::get<pending_t>(data_));
              data_ = std::move(e);

              auto& new_e = std::get<std::exception_ptr>(data_);

              for (auto& cb : p.on_error_) {
                cb(new_e);
              }
            });
        break;
      case Future_State::fullfilled:
        data_ = true;
        break;
      case Future_State::errored:
        data_ = parent_->exception();
      default:
        break;
    }
  }

  Future_State state() const {
    switch (data_.index()) {
      case 0:
        return Future_State::pending;
      case 1:
        return Future_State::fullfilled;
      case 2:
        return Future_State::errored;
      default:
        break;
    }
    return Future_State::invalid;
  }

  std::exception_ptr exception() {
    return std::move(std::get<std::exception_ptr>(data_));
  }

  void add_handlers(on_ready_t r, on_error_t e) {
    assert(r && e);
    assert(state() == Future_State::pending);

    auto& p = std::get<pending_t>(data_);

    p.on_ready_.emplace_back(std::move(r));
    p.on_error_.emplace_back(std::move(e));
  }

  std::mutex mutex_;

  std::variant<pending_t, bool, std::exception_ptr> data_;

  std::shared_ptr<Future_storage<void>> parent_;
};

template <typename T>
struct Future_storage_selector {
  using store_type = Future_storage<T>;
  using future_type = Future<T>;
};

template <typename T>
struct Future_storage_selector<Future_storage<T>> {
  using store_type = Future_storage<T>;
  using future_type = Future<T>;
};

template <typename T>
struct Future_storage_selector<Shared_future_storage<T>> {
  using store_type = Shared_future_storage<T>;
  using future_type = Shared_future<T>;
};

template <typename T>
struct Future_storage_selector<Future<T>> {
  using store_type = Future_storage<T>;
  using future_type = Future<T>;
};

template <typename T>
struct Future_storage_selector<Shared_future<T>> {
  using store_type = Shared_future_storage<T>;
  using future_type = Shared_future<T>;
};


template <typename RES_T>
struct Future_completer {
  template <typename ARG_T, typename CB_T, typename DST_T>
  static void exec(const CB_T& cb, ARG_T arg, DST_T& dst) {
    dst.fullfill(cb(std::move(arg)));
  }
};

template <>
struct Future_completer<void> {
  template <typename ARG_T, typename CB_T, typename DST_T>
  static void exec(const CB_T& cb, ARG_T arg, DST_T& dst) {
    cb(std::move(arg));
    dst.fullfill();
  }
};

template <typename T>
class Future {
 public:
  Future(std::shared_ptr<Future_storage<T>> store) : store_(store) {}

  Future() = default;
  Future(Future&&) = default;
  Future(const Future&) = delete;
  Future& operator=(Future&&) = default;
  Future& operator=(const Future&) = delete;

  template <typename F_T>
  auto then(F_T fn, concur::Work_pool* dst = concur::Work_pool::current()) {
    using func_result_t = std::invoke_result_t<F_T, T>;
    using sel_t = Future_storage_selector<func_result_t>;

    using store_t = typename sel_t::store_type;
    using fut_t = typename sel_t::future_type;

    auto res_fs = std::make_shared<store_t>();

    auto ann = dst->announce_job();

    std::lock_guard l(store_->mutex_);

    auto c_job = [ann{std::move(ann)}, res_fs, fn{std::move(fn)}](T v) mutable {
      ann.push([v{std::move(v)}, res_fs{std::move(res_fs)}, fn{std::move(fn)}] {
        Future_completer<func_result_t>::exec(fn, std::move(v), *res_fs);
      });
    };

    auto f_job = [res_fs](std::exception_ptr e) {
      res_fs->fullfill(std::move(e));
    };

    switch (store_->state()) {
      case Future_State::pending:
        store_->set_handlers(std::move(c_job), std::move(f_job));
        break;
      case Future_State::fullfilled:
        c_job(std::move(store_->data()));
        break;
      case Future_State::errored:
        f_job(store_->exception());
      default:
        break;
    }

    return fut_t(res_fs);
  }

  std::shared_ptr<Future_storage<T>> store_;
};

template <typename RES_T>
struct Void_future_completer {
  template <typename CB_T, typename DST_T>
  static void exec(const CB_T& cb, DST_T& dst) {
    dst.fullfill(cb());
  }
};

template <>
struct Void_future_completer<void> {
  template <typename CB_T, typename DST_T>
  static void exec(const CB_T& cb, DST_T& dst) {
    cb();
    dst.fullfill();
  }
};

template <>
class Future<void> {
 public:
  Future(std::shared_ptr<Future_storage<void>> store) : store_(store) {}

  Future() = default;
  Future(Future&&) = default;
  Future& operator=(Future&&) = default;

  Future(const Future&) = delete;
  Future& operator=(const Future&) = delete;

  template <typename F_T>
  auto then(F_T fn, concur::Work_pool* dst = concur::Work_pool::current()) {
    using func_result_t = std::invoke_result_t<F_T>;
    using sel_t = Future_storage_selector<func_result_t>;

    using store_t = typename sel_t::store_type;
    using fut_t = typename sel_t::future_type;

    auto res_fs = std::make_shared<store_t>();

    auto ann = dst->announce_job();

    std::lock_guard l(store_->mutex_);

    auto c_job = [ann{std::move(ann)}, res_fs, fn{std::move(fn)}]() mutable {
      ann.push([res_fs{std::move(res_fs)}, fn{std::move(fn)}] {
        Void_future_completer<func_result_t>::exec(fn, *res_fs);
      });
    };

    auto f_job = [res_fs](std::exception_ptr e) {
      res_fs->fullfill(std::move(e));
    };

    switch (store_->state()) {
      case Future_State::pending:
        store_->set_handlers(std::move(c_job), std::move(f_job));
        break;
      case Future_State::fullfilled:
        c_job();
        break;
      case Future_State::errored:
        f_job(store_->exception());
      default:
        break;
    }

    return fut_t(res_fs);
  }

  bool valid() const { return !!store_; }

  std::shared_ptr<Future_storage<void>> store_;
};

template <typename T>
class Shared_future {
 public:
  Shared_future(Future<T>&& f) {
    store_ = std::make_shared<Shared_future_storage<T>>(std::move(f.store_));
  }

  Shared_future() = default;
  Shared_future(Shared_future&&) = default;
  Shared_future(const Shared_future&) = default;
  Shared_future& operator=(Shared_future&&) = default;
  Shared_future& operator=(const Shared_future&) = default;

  template <typename F_T>
  auto then(F_T fn, concur::Work_pool* dst = concur::Work_pool::current()) {
    using func_result_t = std::invoke_result_t<F_T, T>;
    using sel_t = Future_storage_selector<func_result_t>;

    using store_t = typename sel_t::store_type;
    using fut_t = typename sel_t::future_type;

    auto res_fs = std::make_shared<store_t>();

    auto ann = dst->announce_job();

    std::lock_guard l(store_->mutex_);

    auto s = store_;
    auto c_job = [ann{std::move(ann)}, s{std::move(s)}, res_fs,
                  fn{std::move(fn)}](const T&) mutable {
      ann.push([s{std::move(s)}, res_fs{std::move(res_fs)}, fn{std::move(fn)}] {
        if constexpr (std::is_same_v<func_result_t, void>) {
          fn(s->data());
          res_fs->fullfill();
        } else {
          res_fs->fullfill(fn(s->data()));
        }
      });
    };

    auto f_job = [res_fs](std::exception_ptr e) {
      res_fs->fullfill(std::move(e));
    };

    switch (store_->state()) {
      case Future_State::pending:
        store_->add_handlers(std::move(c_job), std::move(f_job));
        break;
      case Future_State::fullfilled:
        c_job(std::move(store_->data()));
        break;
      case Future_State::errored:
        f_job(store_->exception());
      default:
        break;
    }

    return fut_t(res_fs);
  }

  bool valid() const { return !!store_; }

  const T& data() const { return store_->data(); }

 private:
  std::shared_ptr<Shared_future_storage<T>> store_;
};

template <>
class Shared_future<void> {
 public:
  Shared_future(Future<void>&& f) {
    store_ = std::make_shared<Shared_future_storage<void>>(std::move(f.store_));
  }

  Shared_future() = default;
  Shared_future(Shared_future&&) = default;
  Shared_future(const Shared_future&) = default;
  Shared_future& operator=(Shared_future&&) = default;
  Shared_future& operator=(const Shared_future&) = default;

  template <typename F_T>
  auto then(F_T fn, concur::Work_pool* dst = concur::Work_pool::current()) {
    using func_result_t = std::invoke_result_t<F_T>;
    using sel_t = Future_storage_selector<func_result_t>;

    using store_t = typename sel_t::store_type;
    using fut_t = typename sel_t::future_type;

    auto res_fs = std::make_shared<store_t>();

    auto ann = dst->announce_job();

    std::lock_guard l(store_->mutex_);

    auto c_job = [ann{std::move(ann)}, res_fs, fn{std::move(fn)}]() mutable {
      ann.push([res_fs{std::move(res_fs)}, fn{std::move(fn)}] {
        if constexpr (std::is_same_v<func_result_t, void>) {
          fn();
          res_fs->fullfill();
        } else {
          res_fs->fullfill(fn());
        }
      });
    };

    auto f_job = [res_fs](std::exception_ptr e) {
      res_fs->fullfill(std::move(e));
    };

    switch (store_->state()) {
      case Future_State::pending:
        store_->add_handlers(std::move(c_job), std::move(f_job));
        break;
      case Future_State::fullfilled:
        c_job();
        break;
      case Future_State::errored:
        f_job(store_->exception());
      default:
        break;
    }

    return fut_t(res_fs);
  }

 private:
  std::shared_ptr<Shared_future_storage<void>> store_;
};

template <typename T>
Shared_future<T> shared(Future<T> v) {
  return Shared_future<T>(std::move(v));
}

template <typename T>
struct is_shared_future : public std::false_type {};

template <typename T>
struct is_shared_future<Shared_future<T>> : public std::true_type {};
}  // namespace slt
#endif