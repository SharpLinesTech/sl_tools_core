#ifndef SLT_CONCUR_TASK_H
#define SLT_CONCUR_TASK_H

#include <memory>
#include "slt/log.h"

namespace slt {
namespace concur {

// This is similar to a std::function<void(...)>, which the exception that it
// does not require the content to be CopyConstructible.
template <typename... args_t>
class Job {
 public:
  Job(std::nullptr_t) {}

  template <typename Callable>
  Job(Callable work)
      : impl_(std::make_unique<Impl_t<Callable>>(std::move(work))) {}

  Job(Job&&) = default;
  Job& operator=(Job&&) = default;

  Job(const Job&) = delete;
  Job& operator=(const Job&) = delete;

  operator bool() const { return !!impl_; }

  template <typename... call_args_t>
  void operator()(call_args_t&&... a) const {
    impl_->call(std::forward<call_args_t>(a)...);
  }

 private:
  class Impl_base {
   public:
    virtual ~Impl_base() {}
    virtual void call(args_t... a) = 0;
  };

  template <typename T>
  class Impl_t : public Impl_base {
   public:
    Impl_t(T&& work) : work_(std::move(work)) {}
    void call(args_t... a) override { work_(std::forward<args_t>(a)...); }

    T work_;
  };

  std::unique_ptr<Impl_base> impl_;
};

}  // namespace concur
}  // namespace slt

#endif