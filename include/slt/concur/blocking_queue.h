#ifndef SLT_CONCUR_BLOCKING_QUEUE_H
#define SLT_CONCUR_BLOCKING_QUEUE_H

#include "slt/log.h"

#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>

namespace slt {
namespace concur {

// A simple mutli-threaded blocking queue.
template <typename T>
class Blocking_queue {
 public:
  template <typename U>
  inline std::size_t push(U&& item) {
    std::unique_lock<std::mutex> lock(mutex_);
    queue_.emplace(std::forward<U>(item));
    std::size_t result = queue_.size();
    lock.unlock();
    cond_.notify_one();

    return result;
  }

  T pop() {
    std::unique_lock<std::mutex> lock(mutex_);
    cond_.wait(lock, [this]{return !queue_.empty();});
    
    auto item = std::move(queue_.front());
    queue_.pop();
    return item;
  }

  inline void clear() {
    std::unique_lock<std::mutex> lock(mutex_);
    queue_ = std::queue<T>();
  }

 private:
  std::queue<T> queue_;
  std::mutex mutex_;
  std::condition_variable cond_;
};
}  // namespace concur
}  // namespace slt

#endif
