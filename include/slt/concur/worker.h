#ifndef SLT_CONCUR_WORKER_H
#define SLT_CONCUR_WORKER_H

#include "slt/concur/blocking_queue.h"
#include "slt/concur/event_queue.h"
#include "slt/concur/task.h"

#include <functional>
#include <thread>

namespace slt {
namespace concur {
class Worker {
 public:
  Worker(Job_queue* src);
  ~Worker();

  void main();

 private:
  Job_queue* src_;
  std::thread thread_;
  std::atomic<bool> term_ = false;
};

}  // namespace concur
}  // namespace slt

#endif
