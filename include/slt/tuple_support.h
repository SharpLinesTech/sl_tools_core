#ifndef SLT_SCENERY_TUPLE_SUPPORT_H
#define SLT_SCENERY_TUPLE_SUPPORT_H

namespace slt {

template <typename Tuple, typename F, std::size_t... Indices>
void for_each_impl(Tuple&& tuple, F&& f, std::index_sequence<Indices...>) {
  using swallow = int[];
  (void)swallow{
      1, (f(std::get<Indices>(std::forward<Tuple>(tuple))), void(), int{})...};
}

template <typename Tuple, typename F>
void for_each(Tuple&& tuple, F&& f) {
  constexpr std::size_t N =
      std::tuple_size<std::remove_reference_t<Tuple>>::value;
  for_each_impl(std::forward<Tuple>(tuple), std::forward<F>(f),
                std::make_index_sequence<N>{});
}

template <typename T, typename Tuple>
struct tuple_type_index;

template <typename T, typename... Rest>
struct tuple_type_index<T, std::tuple<T, Rest...>> {
  static constexpr std::size_t value = 0;
};

template <typename T, typename U, typename... Rest>
struct tuple_type_index<T, std::tuple<U, Rest...>> {
  static constexpr std::size_t value =
      1 + tuple_type_index<T, std::tuple<Rest...>>::value;
};



template<typename prefix, typename tup>
struct tuple_prepend;

template<typename prefix, typename... tup_t>
struct tuple_prepend<prefix, std::tuple<tup_t...>> {
  using type = std::tuple<prefix, tup_t...>;
};

}  // namespace slt

#endif